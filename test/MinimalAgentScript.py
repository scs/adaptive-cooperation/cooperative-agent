import uuid
import os
import time
import sys, getopt
import cooperative_agent as ca
from copy import copy

global quit

config_filename = "minimal_config.json"

def main():
    global quit

    # logging and scenario setup
    # ca.Logger.doLogging = 1
    update_delay = 0.1  # seconds

    # config setup
    if config_filename is not None:
        my_path = os.path.dirname(os.path.abspath(__file__))
        agent_config = ca.Config(my_path, config_filename)
        storage = agent_config.read_config_storage()
        if storage is not None:
            if not agent_config.config_layer_from_storage():
                print("Layer config not available in DB")
                sys.exit(1)
            if not agent_config.config_parameters_from_storage():
                print("Parameter config not available in DB")
                sys.exit(1)
        else:
            print("Config storage could not be loaded:", config_filename)
    else:
        print("no config_filename set!")
        sys.exit(1)

    # agent setup
    my_agent = ca.Hierarchy(agent_config)

    # start update thread
    my_agent.run()
    my_agent.enable_updates()
    # my_agent.disable_updates()

    quit = False
    while not quit:
        try:
            unique_loop_id = uuid.uuid4().hex

            """ model output
            """
            _model_output = my_agent.get_output()  # receive hierarchy output

            """ model input
            """
            my_agent.set_input(unique_loop_id, {"vision": [_model_output, update_delay]})

            ca.sleep(update_delay)

        except KeyboardInterrupt:
            quit = True
            my_agent.set_control_input({"quit": True})

    print("wait to end hierarchy updates")
    ca.sleep(1.)


    sys.exit()

if __name__ == "__main__":
    main()