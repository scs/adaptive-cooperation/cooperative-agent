import time, logging, signal
logging.basicConfig(level=logging.DEBUG)

import random, argparse
import sys
import os

from cooperative_agent import Config
from cooperative_agent.agent import Agent

AGENT_CONFIGS = {
    0: "conf_agent0.json",
    1: "conf_agent1.json"
}

def parse_arguments():
    parser = argparse.ArgumentParser(description="Cooperative agent based on hierarchical processing.")
    parser.add_argument('id', nargs="?", type=int, default=0)                                     
    args = parser.parse_args()
    return args

if __name__ == "__main__":

    args = parse_arguments()

    """

    Agent Config Setup

    """

    logging.getLogger('cooperative_agent.io').setLevel(logging.ERROR)
    logging.getLogger('connectionManager').setLevel(logging.ERROR)
    logging.getLogger('cooperative_agent.agent').setLevel(logging.DEBUG)
    logging.getLogger('cooperative_agent.layer').setLevel(logging.DEBUG)
    logging.getLogger('cooperative_agent.hierarchy').setLevel(logging.DEBUG)
    logging.getLogger('cooperative_agent.world_knowledge').setLevel(logging.ERROR)
    logging.getLogger('cooperative_agent.tom').setLevel(logging.DEBUG)


    config_filename = AGENT_CONFIGS[args.id]
    logging.info("loading config for agent {}".format(args.id))
    path = "configs"
    # agent config setup
    if config_filename is not None:
        my_path = os.path.dirname(os.path.abspath(__file__))
        my_path += os.sep + path

        agent_config = Config(my_path, config_filename)
        storage = agent_config.read_config_storage()
        if storage is not None:
            if not agent_config.config_layer_from_storage():
                logging.error("Layer config not available in DB")
                sys.exit(1)
            if not agent_config.config_parameters_from_storage():
                logging.error("Parameter config not available in DB")
                sys.exit(1)
        else:
            logging.error("Config storage could not be loaded:", config_filename)
            sys.exit(1)
    else:
        logging.error("no config_filename set!")
        sys.exit(1)

    my_agent = Agent(agent_config)
    print(my_agent)

    for sig in (signal.SIGABRT, signal.SIGINT, signal.SIGTERM):
        signal.signal(sig, my_agent.clean_up)

    while True:
    #     player_id = 1
    #     action = random.choice(("Up","Down","Left","Right","Interact"))
    #     io.send({"type": "agentAction", "id": player_id, "action": action})
        time.sleep(0.1)


