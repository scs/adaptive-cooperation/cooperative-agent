# Cooperative Agent

maintained by *Jan Pöppel* (jpoeppel@techfak.uni-bielefeld.de)

This repositories holds the cooperative agent implementing HAICA (publication still under review) in the Overcooked domain (https://gitlab.ub.uni-bielefeld.de/scs/adaptive-cooperation/overcooked-environment).
It can communicate via middleware with the environment, or be instantiated directly. 

## Supported operating systems

Linux, Windows, Mac

## Build dependencies

Python3

## Run dependencies

- Python3
- numpy
- scipy

Additional for distributed usage:

- connectionManager (https://gitlab.ub.uni-bielefeld.de/jpoeppel/connectionmanager)

## Build/Install instructions

So far this has not been intended as a standalone installable.

## Run instructions

You can run the agent either by instantiating it in the environment directly, or by running the `main.py`.
The main supports an argument "id" which sets the preferred ID of the agent within the environment. The environment my assign
a different ID though, e.g. in case the ID was already taken. 


This repository also contains the supervisor file to start the overcooked environment, the webviz gui and two agents automatically.
For this you use `supervisord -c supervisor.conf`. The state of the programs can then be checked using the `supervisorctl`.
