

class Recipes(object):

    def __init__(self):
        self.recipes = dict()
        self.fill_recipes()


    def fill_recipes(self):
        self.recipes["Onion"] = {
            "ingredients": ("Onion", 3),
            "method": "pot"
        }
        self.recipes["Tomato"] = {
            "ingredients": ("Tomato", 3),
            "method": "pot"
        }

    def pot_full(self, pot_obj):
        if not pot_obj["occupied_by"]:
            return False 
        soup = pot_obj["occupied_by"]
        return soup["n_ingredients"] >= self.recipes[soup["flavor"]]["ingredients"][1]

    def requires_ingredient(self, orders, ingredient):
        
        return any(ingredient in self.recipes[order]["ingredients"] for order in orders)


    def requires(self, goal, intention, pots):
        if intention.ID in ("get_item", "deliver_soup", "hand_over"):
            if intention.modifier and (goal.ID in intention.modifier):
                return True 
            if intention.modifier and intention.modifier == "Dish":
                for pot in pots:
                    if pot["occupied_by"] and pot["occupied_by"]["flavor"] == goal.ID:
                        return True 
                else:
                    return False
            if intention.modifier and "Soup-Dish" in intention.modifier:
                return goal.ID in intention.modifier

        if intention.ID == "drop_item":
            if goal.ID not in intention.modifier or intention.modifier == "Dish":
                return True 
        if intention.ID == "interact_with_pot":
            for pot in pots:
                if pot["pos"] == intention.modifier[1]:
                    if not pot["occupied_by"] or pot["occupied_by"]["flavor"] == goal.ID:
                        return True 
            else:
                return False
            # return True
        return False 


class FixedRecipe(object):

    def __init__(self, name):
        self.name = name
        self.steps = []

    def add_step(self, step):
        self.steps.append(step)

class RecipeStep(object):

    def __init__(self, name, pre_conditions, effect):
        self.name = name
        self.pre_conditions = set(pre_conditions)
        self.effect = effect


class FixedRecipes(object):


    @staticmethod
    def create_tomato_soup(self):
        tomato_soup = FixedRecipe("Tomato")
        tomato_soup.add_step(RecipeStep("fill_pot", [("pot_full", None, "not")], "pot_full"))
        tomato_soup.add_step(RecipeStep("cook_soup", [("pot_full", "tomato")], "pot_ready"))
        tomato_soup.add_step(RecipeStep("deliver_soup", [("pot_ready", "tomato")], ""))
        