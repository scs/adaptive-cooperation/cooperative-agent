""" goallayer
Contains the class of the goal layer of the agent's processing hierarchy.

Created on 12.12.2019

@author: skahl
"""

from . import register, Layer



@register
class GoalLayer(Layer):

    def __init__(self, name="goal", log_color="Blue"):
        super(GoalLayer, self).__init__(name, log_color)

        # self.receive_evidence = self.receive_prediction

    
