from collections import deque
import logging
logger = logging.getLogger(__name__)
import numpy as np

import random

from .tom import TheoryOfMind
from .world_knowledge import GridWorld
from .recipe_knowledge import Recipes
from .hierarchy import Hierarchy
from .io import IOControl


class PersonModel(dict):
    def __init__(self):
        self.goal = {}
        self.intention = {}

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: {}".format(name))

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: {}".format(name))


class LayerState(object):

    def __init__(self, domain):
        # Move this to another function to clear without code duplication
        self._init(domain)

    def _init(self, domain):
        self.domain = list(domain)
        self.P = np.array([1/len(domain) for i in range(len(domain))])
        self.id2idx = {hypothesis:idx for idx, hypothesis in enumerate(domain)}
        self.idx2id = {idx: _id for _id, idx in self.id2idx.items()}

    def P_by_ID(self, ID):
        """ Accessing a layers probability by ID should look as follows:
        
        example:
        p = state[layer_name].P_by_ID(id)
        """
        return self.P[self.id2idx.get(ID, None)]

    def P_by_idx(self, idx):
        return self.P[idx]

    def get_MAP(self):
        max_idx = np.argmax(self.P)
        return self.domain[max_idx]

    def sample(self):
        p = random.random()
        u = 0
        for out in self.domain:
            u += self.P_by_ID(out)
            if u >= p:
                break
        return out 

    def clear(self):
        self._init([])

    def impulse(self, _id, strength):
        # Influences the given ID of the layer via the strength
        # before renormalizing
        logger.debug("give impulse of {} to {}. IDs {}. DOmain: {}".format(strength, _id, self.id2idx, self.domain))
        self.P[self.id2idx[_id]] *= strength
        self.normalize()


    def to_dict(self):
        return {
            str(_id): self.P[self.id2idx[_id]] for _id in self.id2idx
        }

    def normalize(self):
        self.P /= sum(self.P) if sum(self.P) != 0 else self.P

    def __repr__(self):
        return ", ".join(["{}: {}".format(out, val) for out,val in zip(self.domain, self.P)])

    def initialize(self, state=None):
        # (Re-)Normalize to equal distribution
        self.P = (self.P*0 + 1)/len(self.P)
        if state:
            # Update given states
            for out in state.domain:
                if out in self.domain:
                    self.P[self.id2idx[out]] = state.P[state.id2idx[out]]
            # Renormalize
            self.normalize()

    def contains(self, outcome, check_modifier=False):
        for out in self.domain:
            if check_modifier:
                if outcome == out.modifier:
                    return True 
            if outcome == out.ID:
                return True
        return False

    def clear_targets(self):
        for out in self.domain:
            out.clear()

class DomainObject(object):
    def __init__(self, hypo, modifier=None):
        self.ID = hypo
        self.modifier = modifier
        self.target = None

    def __repr__(self):
        mod = "({})".format(self.modifier) if self.modifier else ""
        return "{}{}".format(self.ID, mod)

    def clear(self):
        self.target = None

    def copy(self):
        return DomainObject(self.ID, self.modifier)

    def __eq__(self, other):
        return self.ID == other.ID and self.modifier == other.modifier

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return hash(self.__repr__())


class Agent(PersonModel):

    def __init__(self, config):
        super(Agent, self).__init__()
        self.other = None
        self.tom = TheoryOfMind({"goal": self.goal_domain, "intention": self.full_intention_domain})
        self.layer_domains = None
        self.intented_action = None
        self.intended_intention = None
        self.last_agent_pos = None
        self.last_agent_hands = None
        self.last_agent_orientation = None
        

        # SETUP
        self.config = config.parameters
        self.hierarchy = self.configure_hierarchy(config)
        self.my_id = self.hierarchy.my_id
        self.env_id = None
        """ 

        Network IO Config Setup

        """
        own_port = config.parameters["port"]
        logger.info("own port from config: {}".format(own_port))
        env_port = config.parameters["env_port"]
        io_config = {
            "ctrl_port": own_port, # Port on which the agent server should listen
            "init_params": {        # Parameters for the message to the model to setup the connection
                            "protocol": "tcp",
                            "server_name": "localhost",
                            "port": env_port,
                            "ident": self.my_id     
                            },
            "desired_agent_id": self.my_id,
            "identifier": "agent{}".format(self.my_id), # The identifier the model uses to distinguish this connection from others
        }
        self.use_io = self.config.get("use_io", True)
        if self.use_io:
            self.io = IOControl(self, self.setup_player_connection, config=io_config)
            self.io.callback = self.step


        self.layer_domains = {'goal': self.goal_domain, 'intention': []} #self.intention_domain} # Note: Leave empty for now as we will fill it form observations
        self.state = self.prep_state(list(self.hierarchy.layername_idx_dict.keys()), 
                                                                self.layer_domains)
        logger.debug("agent state: {}".format(self.state))
        self.stats = self.prep_stats(list(self.hierarchy.layername_idx_dict.keys()))

        # Fill the other's state with all possible intentions
        self.other = {
                "goal": {goal: 1/len(self.goal_domain) for goal in self.goal_domain},
                "intention": None #{key: 1/len(self.full_intention_domain) for key in self.full_intention_domain}
            }


        self.recipe_knowledge = Recipes()
        self.world_knowledge = GridWorld(self.recipe_knowledge)

        """ layer-P-dist access tests
        """
        logger.debug("Tests:\n1) print each P from layer 'goal'")
        for hypo in self.layer_domains['goal']:
            logger.debug("{}, {}".format(hypo.ID, self.state['goal'].P_by_ID(hypo)))
        logger.debug("2) print all P from layer 'intention'")
        logger.debug("intention.P: {}".format(self.state['intention'].P))


    def clean_up(self, *args):
        logger.info("Cleaning up agent before shutting down")
        if self.use_io:
            self.io.clean_up(self.env_id)
        import os 
        os._exit(0)

    @property
    def goal_domain(self):
        return [DomainObject('Tomato'), DomainObject('Onion')]

    @property
    def intention_domain(self):
        return [
            DomainObject("get_item"),
            DomainObject("interact_with_pot"),
            DomainObject("deliver_soup"),
            DomainObject("hand_over"), # TODO FIX
            DomainObject("drop_item"),
            DomainObject("wait")
        ]

    @property
    def full_intention_domain(self):
        return [
            DomainObject("wait"),
            DomainObject("get_item", "Onion"),
            DomainObject("get_item", "Tomato"),
            DomainObject("get_item", "Dish"),
            DomainObject("get_item", "Soup-Dish"),
            DomainObject("interact_with_pot"),
            DomainObject("drop_item", "Onion"),
            DomainObject("drop_item", "Tomato"),
            DomainObject("drop_item", "Dish"),
            DomainObject("drop_item", "Soup-Dish"),
            DomainObject("deliver_soup", "Tomato"),
            DomainObject("deliver_soup", "Onion"),
            DomainObject("hand_over")
        ]


    def __repr__(self):
        repr_str = "Agent hierarchy:"
        repr_str += str(self.hierarchy.layers)
        return repr_str


    def setup_player_connection(self, agent_id):
        logger.info("setting env id to: {}".format(agent_id))
        self.env_id = int(agent_id)

    def configure_hierarchy(self, config):
        hierarchy = Hierarchy(config) 
        self.my_id = hierarchy.my_id

        hierarchy.get_layer("intention").likelihood_bottom_up = self.intention_likelihoods_bu
        hierarchy.get_layer("intention").likelihood_top_down = self.intention_likelihoods_td
        hierarchy.get_layer("goal").likelihood_bottom_up = self.goal_likelihoods_bu
        hierarchy.get_layer("goal").likelihood_top_down = self.goal_likelihoods_td
        
        # ...
        # layer_1 = Layer("recipes")
        # layer_1.likelihood_top_down = self.recipe_top_down
        # layer_1.likelihood_bottom_up = self.recipe_bottom_up
        # layer.set_top_down = self.compute_likelihood
        # layer.set_bottom_up = self.compute_bottom_up_likelihood
    
        return hierarchy


    def prep_state(self, layer_names, layer_domains):
        """ Prepare state-struct that consists of layer states.
        This preparation expects a list of layer names, as well as a dict of layer domains, 
        with each domain consisting of a list of possible hypotheses.
        """
        state = {name: 
            LayerState(
                domain = layer_domains[name]
            )
            for name in layer_names
        }
        return state


    def prep_stats(self, layer_names, hist_len=10):
        stats = {name:
            {
                'free_energy': 0.,
                'transient_FE': deque(maxlen=hist_len), 
                'mean_FE': 0.,
                'precision': 0.,
                'K': 0.,
                'ToM_cooperativeness': self.config.get("ToM_cooperativeness", 0.),
                'ToM_dominance': self.config.get("ToM_dominance", 0.),
                'ToM_K': self.config.get("ToM_K", 0.),
            }
            for name in layer_names
        }

        return stats

    def _check_intentions(self, observations):
        
        item_types = self.world_knowledge.get_item_types()
        intentions_to_add = []
        for item in item_types:
            if not self.state["intention"].contains(item, check_modifier=True):
                intentions_to_add.append(item)

        new_domain = list(self.state["intention"].domain)
        if intentions_to_add:
            for intention in self.intention_domain:
                # Create its own intention for all possible items for now
                if intention.ID in ("get_item", "drop_item") or (intention.ID == "hand_over" and self.world_knowledge.more_agents(self.env_id)):
                    for item in intentions_to_add:
                        if not item in ("Soup-Cooking"):
                            tmp_int = intention.copy()
                            tmp_int.modifier = item
                            new_domain.append(tmp_int)  
                elif intention.ID == "deliver_soup":
                    for item in intentions_to_add:
                        if not item in ("Soup", "Soup-Dish-Tomato", "Soup-Dish-Onion", "Soup-Cooking", "Dish"):
                            tmp_int = intention.copy()
                            tmp_int.modifier = item
                            new_domain.append(tmp_int)
                elif intention.ID == "interact_with_pot":
                    pots_to_consider = self.world_knowledge.get_pots()
                    interact_intention = DomainObject("interact_with_pot")
                    for pot in pots_to_consider:
                        tmp_int = interact_intention.copy()
                        tmp_int.modifier = ("pot", pot["pos"]) 
                        if not self.state["intention"].contains(tmp_int.modifier, check_modifier=True):
                            new_domain.append(tmp_int)
                # Other intentions do not need to be adapted for the items
                else:
                    if not intention in new_domain:
                        new_domain.append(intention)

            logger.debug("new_domain: {}".format(new_domain))

            self.layer_domains["intention"] = new_domain
            self.tom.domains["intention"] = new_domain

            for key in new_domain:
                if self.other and self.other["intention"] and not key in self.other["intention"]:
                    self.other["intention"][key] = 0
                
            new_state = LayerState(new_domain)
            new_state.initialize(self.state["intention"])
            self.state["intention"] = new_state

    def _check_success(self, observations):
        reward = observations["reward"]
        if reward > 0: # Assume positive reward on success
            self.reset_state()

    def send_state(self):
        """
            Serialises the current state and sends a copy out to any listener.
        """

        _state = {key: val.to_dict() for key,val in self.state.items()}
        _state["agent_id"] = self.env_id
        _state["curGoal"] = str(self.state["goal"].get_MAP())
        _state["curIntention"] = str(self.state["intention"].get_MAP())

        _otherState = {var: {str(dom): val for dom,val in self.other[var].items()} for var in self.other} if self.other else None
        state_msg = {"type": "stateMessage", "agent_id": self.env_id, "agentState{}".format(self.env_id): {"self": _state, "other": _otherState}}
        if self.use_io:
            self.io.notify_state_observers(state_msg)


    def reset_state(self):
        logger.debug("resetting state")
        for layer_state in self.state.values():
            layer_state.initialize()
        self.state["intention"].clear()
        # self.other = {var: {out: 1/len(self.other[var]) for out in self.other[var]} for var in self.other} if self.other else None
        self.other = None
        self.intented_action = None 
        self.intended_intention = None


    def step(self, observations):
        if "new_env" in observations["info"]:
            self.reset_state()

        self.world_knowledge.update(observations)

        player = self.world_knowledge.get_player(self.env_id)
        other_player = self.world_knowledge.get_other_player(self.env_id)

        logger.debug("Step with player {}, other {}".format(player, other_player))

        # Check environment for new objects, requiring potentially new intentions
        self._check_success(observations)
        self._check_intentions(observations)


        if not self._check_intended_action():
            # Have a chance of trying to wait once
            if random.random() > 0.5:
                logger.debug("waiting due to unsuccessful action")
                self.intented_action = "Wait"
                return self.env_id, "Wait"
        # else: # TODO For later when we revisit adaptive ToM
        #     if self.intented_action == "Interact":
        #         # Assume intention was done
        #         self.tom.evaluate_last_intention(self.intended_intention)


        # Consider having the ToM update to be run in its own parallel thread?
        if self.config.get("perform_ToM", True):
            self.other = self.tom.update(self.other, self.world_knowledge, self.env_id)
        else:
            self.other = None

        # TODO: Update ToM_K somewhere, to incorporate perceived cooperativeness and dominance (default: 0.5)

        # logger.debug("other state: {}".format(self.other))

        orders = list(observations["info"]["orders"])
        order_state = LayerState(domain = [DomainObject(order) for order in orders])
        if self.config.get("Perceive_Orders", True) is False:
            # No order knowledge is equivalent of an equal distribution for all possible orders
            order_state = LayerState(domain = [DomainObject("Tomato"), DomainObject("Onion")])

        # Simulate
        self.state["orders"] = order_state
        self.state["world"] = observations

        # logger.info("State: {}".format(observations))
        # 1. Infer potential other's goals given inferred intentions and last inferred goal distribution
        # 2. Infer potential other's intention using last assumed goal and last inferred intention distribution
        # 3. Infer own goal given own last goal distribution, state and inferred other's goal (weighted with cooperativeness)
        # 4. Infer next intention given last inferred intentions, state, winning own goal and winning other's intention
        # 5. Plan and execute next own action given inferred intention for self (and potentially inferred action for other?)

        layer_states, self.stats = self.hierarchy.update(self.stats, observations, self.state, self.other, self.layer_domains)
        
        del self.state["orders"]
        del self.state["world"]
        # self["goal"] = layer_states["goal"]
        
        # logger.debug("Current intentions: {}".format(self.state["intention"]))
        action = None
        best_intention = self.state["intention"].get_MAP()

        if self.intended_intention and best_intention != self.intended_intention and self.intented_action != "Interact":
            # When we assume that intentions end via an "interact" (true for now)
            # This should only happen if we need to abort our current intention
            # "Punish" old intention to prevent a quick switch back
            self.state["intention"].impulse(self.intended_intention, 0.01)
            if random.random() > 0.5:
                logger.debug("waiting due to aborted intention")
                self.intented_action = "Wait"
                return self.env_id, "Wait"
            # Unfortunately, this does not prevent the agent from cycling between get_item and drop_item/hand_over
            # repeatedly

        self.intended_intention = best_intention

        action, path = self.world_knowledge.plan_action(best_intention, self.env_id)

        self.intented_action = action

        self.state["intention"].clear_targets()

        self.send_state()

        logger.debug("Chose action: {} for intention: {}".format(action, best_intention))

        return self.env_id, action

    def _check_intended_action(self):
        agent = self.world_knowledge.get_player(self.env_id)
        if agent is None:
            logger.error("Could not get agent obj for id: {}. (grid: {})".format(self.env_id, self.world_knowledge.grid))

        if self.last_agent_pos is None:
            x1,y1 = 0,0
        else:
            x1,y1 = self.last_agent_pos
        x2,y2 = agent["pos"]
        d_x, d_y = (x2-x1, y2-y1)

        if (d_x, d_y) == (0,0) and self.last_agent_orientation != tuple(agent["orientation"]):
            d_x, d_y = agent["orientation"]

        action = None
        if (d_x, d_y) == (1,0):
            action = "Right"
        if (d_x, d_y) == (-1,0):
            action = "Left"
        if (d_x, d_y) == (0,1):
            action = "Down"
        if (d_x, d_y) == (0,-1):
            action = "Up"
        if (d_x, d_y) == (0, 0):
            if self.last_agent_hands == agent["in_hands"]:
                action =  "Wait"
            else:
                action = "Interact"

        self.last_agent_pos = (x2, y2)
        self.last_agent_hands = agent["in_hands"]
        self.last_agent_orientation = tuple(agent["orientation"])
        
        return (action == self.intented_action) if self.intented_action else True

    def goal_likelihoods_bu(self, goal_domain, intention_state):
        # state is LayerState from intention layer
        # Computes P(goal|intention) for all goals and all intentions!
        # Returns a dict containing the "soft-evidence" likelihood for
        # each goal
        res = {}
        for out in goal_domain:
            res[out] = sum([self.goal_likelihood_bu(out, intention)*intention_state.P_by_ID(intention) 
                                for intention in intention_state.domain])
        # Re-normalize just in case
        norm = sum(res.values())
        res = {k: v/norm for k,v in res.items()}
        return res

    def goal_likelihood_bu(self, goal, intention):
        # Computes P(goal|intention)

        # TODO consider making this smarter and more 
        # fine grained?
        if self.recipe_knowledge.requires(goal, intention, self.world_knowledge.get_pots()):
            return 1
        return 0

    def goal_likelihoods_td(self, domain, actual_order):
        # We do not need to use soft evidence here, because we get "hard" evidence in
        # the form of our observations
        res = {out: self.goal_likelihood_td(out, actual_order) for out in domain}
        
        # Re-normalize in case we have multiple orders
        norm = sum(res.values())
        res = {k: v/norm for k,v in res.items()}
        return res

    def goal_likelihood_td(self, goal, order):
        # TODO consider taking the ordering of orders into account, i.e. weigh the first order higher?
        return 1 if order.contains(goal.ID) else 0

    def intention_likelihoods_td(self, intention_domain, goal_state):
        # Computes the likelihood P(intention|goal), treating goal as soft
        # evidence
        pots = self.world_knowledge.get_pots()
        norms = {}
        for goal in goal_state.domain:
            norms[goal] = sum([self.recipe_knowledge.requires(goal, intention, pots) for intention in intention_domain])

        return {out: sum([self.recipe_knowledge.requires(goal, out, pots)/norms[goal]*goal_state.P_by_ID(goal) 
                            for goal in goal_state.domain]) for out in intention_domain}

        

    def intention_likelihoods_bu(self, intention_domain, world_state):
        """
            Computes the (unnormalized) bottom-up likelihood of the given intention.

            Possible Intentions we need to cover:

            Fine grained (without need for cooperation)
            - get first Onion/Tomato
            - get second Onion/Tomato
            - get third Onion/Tomato
            - bring first Onion/Tomato to pot
            - bring second Onion/Tomato to pot
            - bring third Onion/Tomato to pot
            - get plate
            - get soup from pot
            - deliver soup

            When cooperation is necessary additionally
            - place first/second/third Onion/Tomato at reachable position for other player
            - place plate at reachable position for other player
            - pickup soup

            Functional intentions:

            - Get_Item(Item) with item being currently Onion,Tomato,Plate,Soup
                - The goal of this intention would be to hold item in the player's hand
            - Deliver_To_pot(pot)/Interact_with_Pot(pot):
                - Interact with the specified pot with an item in the player's hands
            - Deliver_Soup
                - Requires finished dish in agent hand and way to reach serving area
            - Hand_over(item):
                - Place item somewhere the other agent can reach it (make it easier: Pick closest 
                position, reachable by both)
            - Drop_item():
                Drop the current item at the closest free counter
        """
        # return {out.ID: random.random() for out in intention_domain}
        res = {}

        player_obj = self.world_knowledge.get_player(self.env_id)
        if player_obj is None:
            logger.error("Could not find player for id {}".format(self.env_id))
        for intention in intention_domain:

            # like = self.world_knowledge.likelihood(intention, self.env_id)
            if intention.ID == "get_item":
                like = self.get_item_likelihood(intention.modifier, player_obj)
            if intention.ID == "interact_with_pot":
                like = self.interact_with_pot_likelihood(intention, player_obj)
            if intention.ID == "deliver_soup":
                like = self.deliver_soup_likelihood(intention, player_obj)
            #Consider if we need the modifiers for these likelihood?
            if intention.ID == "hand_over":
                like = self.hand_over_likelihood(intention.modifier, player_obj)
            if intention.ID == "drop_item":
                like = self.drop_item_likelihood(intention.modifier, player_obj)
            if intention.ID == "wait":
                like = self.wait_likelihood(player_obj)

            res[intention] = like


        logger.debug("unnormalized likelihoods: {}".format(res))
        # Normalize
        norm = sum(res.values())
        res = {intention: res[intention]/norm for intention in res}
        # Softmax normalization
        # beta = 3
        # norm = sum([np.exp(beta * res[intention]) for intention in res])
        # res = {intention: np.exp(beta*res[intention])/norm for intention in res}

        

        return res


    def get_item_likelihood(self, item, player_obj):

        if player_obj["in_hands"] != None:
            return 0

        closest_item_pos, dist = self.world_knowledge.get_item_pos(item, player_obj)
        # If player cannot reach the item at all, likelihood goes to 0
        if closest_item_pos is None:
            return 0.01

        if "Soup" in item:
            return 1
        
        # Initialize probability according to distance
        prob = 0 #self.world_knowledge.scale_distance(dist)/2
        # Scale probability by the number of remaining free slots in the pot(s)
        pot_objs = self.world_knowledge.get_pots()
        applicable = []
        ready = []
        for pot in pot_objs:
            if self.recipe_knowledge.pot_full(pot):
                applicable.append(True if item == "Dish" else False)
                if pot["occupied_by"]["ready"]:
                    ready.append(pot)
            else:
                if pot["occupied_by"] and item != pot["occupied_by"]["flavor"]:
                    applicable.append(False)
                else:
                    applicable.append(True if item != "Dish" else False)

                    
        if any(applicable):
            prob = 1
        else:
            prob = 0

        if item == "Dish":
            # logger.debug("prob for dish pickup {}. applicables: {}".format(prob,  applicable))
            if not any(ready):
                prob *= 0.8

        # Reduce probability if other player already holds the item?
        # other_player = self.world_knowledge.get_other_player(player_obj["id"])
        # if other_player and other_player["in_hands"] == item:
        #     prob *= 0.5 #?
        
        return prob

    def interact_with_pot_likelihood_old(self, intention, player_obj):
        # Would this be a possible interaction?
        if player_obj["in_hands"] is None or player_obj["in_hands"]["type"] == "Soup-Dish":
            return 0


        prob = 0
        sel_pot = None
        pot_objs = self.world_knowledge.get_pots()
        for pot in pot_objs:
            if not self.world_knowledge.reachable(player_obj["pos"], pot["pos"]):
                continue
            if not self.recipe_knowledge.pot_full(pot):
                if not pot["occupied_by"] and player_obj["in_hands"]["type"] == "Dish":
                    # Skip this pot
                    continue
                if not pot["occupied_by"] or ((not pot["occupied_by"]["ready"])
                                            and pot["occupied_by"]["flavor"] == player_obj["in_hands"]["type"]):
                    dist, path = self.world_knowledge.compute_distance(tuple(player_obj["pos"]), pot["pos"])
                    p = self.world_knowledge.scale_distance(dist)
                    if p > prob:
                        prob = p
                        sel_pot = pot
            else:
                if pot["occupied_by"]["ready"] and player_obj["in_hands"]["type"] == "Dish":
                    prob = 1
                    sel_pot = pot
                    break
        intention.target = sel_pot
        return prob

    def interact_with_pot_likelihood(self, intention, player_obj):
        if player_obj["in_hands"] is None or player_obj["in_hands"]["type"] == "Soup-Dish":
            return 0
        pot_pos = intention.modifier[1]
        pot = self.world_knowledge.grid[pot_pos]
        pot["pos"] = pot_pos
        prob = 0
        intention.target = pot
        if not self.world_knowledge.reachable(player_obj["pos"], pot["pos"]):
            return 0

        if not self.recipe_knowledge.pot_full(pot):
            if not pot["occupied_by"] and player_obj["in_hands"]["type"] == "Dish":
                prob = 0
            elif (not pot["occupied_by"] and player_obj["in_hands"]):
                dist, path = self.world_knowledge.compute_distance(tuple(player_obj["pos"]), pot["pos"])
                prob = self.world_knowledge.scale_distance(dist)
            elif (pot["occupied_by"] and not pot["occupied_by"]["ready"] and pot["occupied_by"]["flavor"] == player_obj["in_hands"]["type"]):
                dist, path = self.world_knowledge.compute_distance(tuple(player_obj["pos"]), pot["pos"])
                prob = self.world_knowledge.scale_distance(dist)*np.exp(0.4*pot["occupied_by"]["n_ingredients"])
        else:
            if pot["occupied_by"]["ready"] and player_obj["in_hands"]["type"] == "Dish":
                dist, path = self.world_knowledge.compute_distance(tuple(player_obj["pos"]), pot["pos"])
                prob = self.world_knowledge.scale_distance(dist)
        return prob

    
        
    def deliver_soup_likelihood(self, intention, player_obj):
        # Would this be a possible interaction?
        if player_obj["in_hands"] and  player_obj["in_hands"]["type"] == "Soup-Dish":
            if player_obj["in_hands"]["flavor"] != intention.modifier:
                return 0
            else:
                if intention.modifier in self.world_knowledge.orders:
                    return 1
                else:
                    return 0.2

        return 0


    def wait_likelihood(self, player_obj):
        return 0.08


    def hand_over_likelihood(self, item, player_obj):
        if player_obj["in_hands"] is None or player_obj["in_hands"]["type"] != item:
            return 0
        
        other_player = self.world_knowledge.get_other_player(player_obj["id"])
        if other_player is None:
            return 0
        
        both_reachable = self.world_knowledge.shared_reachable(player_obj, other_player)

        if both_reachable is None:
            return 0

        best_pot = 0
        pot_objs = self.world_knowledge.get_pots()
        for pot in pot_objs:
            logger.debug("checking pot: {}".format(pot))
            if (not self.recipe_knowledge.pot_full(pot)) or (item == "Dish" and self.recipe_knowledge.pot_full(pot)):
                dist, path = self.world_knowledge.compute_distance(tuple(player_obj["pos"]), tuple(pot["pos"]))
                p = self.world_knowledge.scale_distance(dist)
                logger.debug("prob for pot: {}".format(p))
                if p > best_pot:
                    best_pot = p 

        prob = 1
        if best_pot != 0:
            # We could reach the pot alone: 
            # prob *= best_pot # TODO invert
            prob = 0.05

        # Reduce probability when the other player has something in hands
        if other_player["in_hands"]:
            prob *= 0.1

        logger.debug("hand over prob: {}".format(prob))

        return prob

    def drop_item_likelihood2(self, item, player_obj):
        if player_obj["in_hands"] is None or player_obj["in_hands"]["type"] != item:
            return 0

        prob = 0.3

        pot_objs = self.world_knowledge.get_pots()
        for pot in pot_objs:
            if self.recipe_knowledge.pot_full(pot):
                break 
        else:
            # All pots are not yet full
            if player_obj["in_hands"]["type"] == "Dish":
                prob = 0.3
        
        other_player = self.world_knowledge.get_other_player(player_obj["id"])
        if other_player and other_player["in_hands"] == player_obj["in_hands"]:
            prob *= 1.5

        return prob

    def drop_item_likelihood(self, item, player_obj):
        # New, simpler likelihood
        if player_obj["in_hands"] is None or player_obj["in_hands"]["type"] not in item:
            return 0

        

        item_useable = False 
        pot_objs = self.world_knowledge.get_pots()
        for pot in pot_objs:
            if self.world_knowledge.useable(pot, item):
                item_useable = True 
                break 

        if "Soup" in item:
            item_useable = True
        
        if item_useable:
            return 0.2
        else:
            return 1
        
        # other_player = self.world_knowledge.get_other_player(player_obj["id"])
        # if other_player and other_player["in_hands"] == player_obj["in_hands"]:
        #     prob *= 1.5

        # return prob
