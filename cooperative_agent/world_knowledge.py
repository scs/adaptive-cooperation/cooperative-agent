
from functools import lru_cache
from heapq import heappop, heappush
import math, random
import logging
logger = logging.getLogger(__name__)

VALID_NEIGHBOURS = [(-1,0), (1,0), (0,-1), (0,1)]

INGREDIENTS = ("Onion", "Tomato")

class GridWorld(object):
    """
        A simple 2d Gridworld representation where tiles are dictionaries 
        containing information regarding the tile. Provides distance functionality.
    """

    def __init__(self, recipe_knowledge):
        self.grid = {}
        self.orders = []
        self.recipe_knowledge = recipe_knowledge

    @classmethod
    def from_state(cls, state):
        res = cls()
        res.update(state)
        return res

    def update(self, state):
        # Reset cache, as things (objects or players) may have changed
        # requiring us to recompute the distances -.-
        # self.compute_distance.cache_clear()
        self.grid.clear()
        self.orders = list(state["info"]["orders"])
        for y, row in enumerate(state["env"]):
            for x, el in enumerate(row):
                self.grid[(x,y)] = el 

    def get_player(self, player_id):
        for el in self.grid.values():
            if el["occupied_by"] and el["occupied_by"]["type"] == "Player": 
                if el["occupied_by"]["id"] == player_id:
                    return el["occupied_by"]
        return None

    def more_agents(self, player_id):
        return self.get_other_player(player_id) != None

    def get_other_player(self, player_id):
        """
            Should return the player object with a different ID as the 
            given id.
        """
        for el in self.grid.values():
            if el["occupied_by"] and el["occupied_by"]["type"] == "Player": 
                if el["occupied_by"]["id"] != player_id:
                    return el["occupied_by"]
        return None

    def get_pots(self):
        res = []
        for pos, el in self.grid.items():
            if el["type"] == "Pot":
                # Add position to pot elements
                el["pos"] = pos
                res.append(el)
        return res

    def get_types(self):
        res = set()
        for el in self.grid.values():
            if el["occupied_by"] and el["occupied_by"]["type"] != "Player":
                res.add(el["occupied_by"]["type"])
            if "Dispenser" in el["type"]:
                res.add(el["type"].replace("Dispenser", "")) 
            if el["type"] not in ("Ground", "Counter"):
                res.add(el["type"])
        return res

    def get_item_types(self):
        res = set()
        for el in self.grid.values():
            if el["occupied_by"] and el["occupied_by"]["type"] != "Player":
                res.add(el["occupied_by"]["type"])
                # if el["occupied_by"]["type"] == "Soup-Dish":
                #     res.add("{}-{}".format(el["occupied_by"]["type"], el["occupied_by"]["flavor"]))
                # else:
                #     res.add(el["occupied_by"]["type"])
            if "Dispenser" in el["type"]:
                res.add(el["type"].replace("Dispenser", "")) 
            # if el["occupied_by"] and el["occupied_by"]["type"] == "Player":
            #     if el["occupied_by"]["in_hands"]:
            #         if el["occupied_by"]["in_hands"]["type"] == "Soup-Dish":
            #             res.add("{}-{}".format(el["occupied_by"]["in_hands"]["type"], el["occupied_by"]["in_hands"]["flavor"]))
            #         else:
            #             res.add(el["occupied_by"]["in_hands"]["type"])
        return res

    def get_items(self, item_type):
        """
            Search the grid for the desired itemtype. The resulting list will
            also include dispensers for the desired type.
        """
        res = []
        for pos, el in self.grid.items():
            if item_type in el["type"]:
                # Add position to dispenser elements
                el["pos"] = pos
                res.append(el) 
            if el["occupied_by"] and item_type in el["occupied_by"]["type"]:
                res.append(el["occupied_by"]) 
        return res 

    def players_close(self, player_id):
        player_obj = self.get_player(player_id)
        other_player = self.get_other_player(player_id)
        if other_player:
            x1,y1 = player_obj["pos"]
            x2,y2 = other_player["pos"]
            if abs(x1-x2) + abs(y2-y2) < 4:
                return True 
        return False 
    
    def get_item_pos(self, item, player_obj):
        item_objs = self.get_items(item)
        min_dist = float("inf")
        res_pos = None
        other_player = self.get_other_player(player_obj["id"])
        ignore_other = not self.players_close(player_obj["id"])
        for obj in item_objs:
            d, path = self.compute_distance(tuple(player_obj["pos"]), tuple(obj["pos"]), direction=player_obj["orientation"],
                                            ignore=None)
            if d is None and ignore_other:
                d, path = self.compute_distance(tuple(player_obj["pos"]), tuple(obj["pos"]), direction=player_obj["orientation"],
                                            ignore=[tuple(other_player["pos"])] if (other_player and ignore_other) else None)
            if d is not None and d < min_dist: 
                min_dist = d
                res_pos = tuple(obj["pos"])

        return res_pos, min_dist

    def plan_get_item(self, player_id, item):
        player_obj = self.get_player(player_id)
        closest_item_pos, dist = self.get_item_pos(item, player_obj)
        if closest_item_pos is None:
            return None, None
        # This call is a little bit doubled since we already compute this when we
        # find the item, but since it is cached, this should be ok
        other_player = self.get_other_player(player_id)
        ignore_other = not self.players_close(player_id)
        dist, path = self.compute_distance(tuple(player_obj["pos"]), tuple(closest_item_pos), direction=player_obj["orientation"],
                                            ignore=None)
        if dist is None and ignore_other:
            dist, path = self.compute_distance(tuple(player_obj["pos"]), tuple(closest_item_pos), direction=player_obj["orientation"],
                                            ignore=[tuple(other_player["pos"])] if (other_player and ignore_other) else None)
        
        return self.deduce_action(player_obj, path, closest_item_pos), path

    def plan_pot(self, player_obj, pot_obj):
        if pot_obj is None:
            return None, None
        logger.debug("plan pot for player {} and pot {}".format(player_obj, pot_obj))
        # This call is a little bit doubled since we already compute this when we
        # find the item, but since it is cached, this should be ok
        other_player = self.get_other_player(player_obj["id"])
        ignore_other = not self.players_close(player_obj["id"])
        dist, path = self.compute_distance(tuple(player_obj["pos"]), pot_obj["pos"], direction=player_obj["orientation"], 
                                            ignore=None)
        if dist is None and ignore_other:
            dist, path = self.compute_distance(tuple(player_obj["pos"]), pot_obj["pos"], direction=player_obj["orientation"], 
                                            ignore=[tuple(other_player["pos"])] if (other_player and ignore_other) else None)
        return self.deduce_action(player_obj, path, pot_obj["pos"]), path

    def plan_hand_over(self, player_id):
        player_a = self.get_player(player_id)
        player_b = self.get_other_player(player_id)
        if player_b is None:
            logger.debug("Hand over. No other player")
            return None, None
        el = self.shared_reachable(player_a, player_b)
        if el:
            dist, path = self.compute_distance(tuple(player_a["pos"]), el["pos"])
            logger.debug("hand over. choose action according to {}".format(path))
            return self.deduce_action(player_a, path, el["pos"]), path
        logger.debug("Hand over. No shared path")
        return None, None

    # def plan_drop_item2(self, player_id):
    #     """
    #         Old version of plan drop_item, that drops at any
    #         free neighbour tile.
    #     """
    #     player_obj = self.get_player(player_id)
    #     # Check if we can just drop it in front of us
    #     px, py = player_obj["pos"]
    #     dx, dy = player_obj["orientation"]
    #     if self.can_drop_on((px+dx,py+dy)):
    #         return "Interact"

    #     # For now just check stupidly for the first free tile to drop it on
    #     # if there is no one around us, we would not be able to move elsewhere anyways
    #     neighbours = [n_pos for n_pos in [(px+x, py+y) for x,y in VALID_NEIGHBOURS]]
    #     for n_pos in neighbours:
    #         if self.can_drop_on(n_pos):
    #             return self.deduce_action(player_obj, [(px,py), n_pos], n_pos)

    #     return None, None

    def plan_drop_item(self, player_id):
        """
            New plan drop item, which will only try to drop items on a free counter space
        """
        player_obj = self.get_player(player_id)
        # Check if we can just drop it in front of us

        closest_counter_pos = None
        best_dist = float("inf")
        best_path = None 
        counters = self.get_free_counters()
        for counter in counters:
            dist, path = self.compute_distance(tuple(player_obj["pos"]), counter["pos"])
            if dist and dist < best_dist:
                best_dist = dist 
                closest_counter_pos = counter["pos"]
                best_path = path 
        if closest_counter_pos is None:
            return None, None
        return self.deduce_action(player_obj, best_path, closest_counter_pos), best_path

    def get_free_counters(self):
        res = []
        for pos, el in self.grid.items():
            if el["type"] == "Counter" and el["occupied_by"] == "":
                el["pos"] = pos
                res.append(el)
        return res

    def reachable(self, start, goal):
        dist, path = self.compute_distance(tuple(start), tuple(goal))
        return path != None

    def plan_to_serving(self, player_id):
        player_obj = self.get_player(player_id)
        px, py = player_obj["pos"]
        counters = self.get_serving_counter()
        best_dist = float('inf')
        best_path = None
        best_counter = None 
        for counter in counters:
            dist, path = self.compute_distance(tuple(player_obj["pos"]), counter["pos"])
            # TODO check for closest counter instead of going to the first possible
            if dist and dist < best_dist:
                best_dist = dist  
                best_path = path
                best_counter_pos = counter["pos"]
        if best_path is not None:
            return self.deduce_action(player_obj, best_path, best_counter_pos), best_path
        return None, None
        
    def get_serving_counter(self):
        counters = []
        for pos, el in self.grid.items():
            if el["type"] == "Serving":
                el["pos"] = pos
                counters.append(el)
        return counters
        
    def can_drop_on(self, tile_pos):
        if "Dispenser" in self.grid[tile_pos]["type"]:
            return False 
        if self.grid[tile_pos]["occupied_by"] != "":
            return False 
        return True

    def deduce_action(self, player, path, target):
        if path is None:
            return None
        next_pos = path[1]
        x,y = player["pos"]
        dir_x,dir_y = player["orientation"]
        if next_pos == target and (x+dir_x, y+dir_y) == next_pos:
            return "Interact"
        d_x = next_pos[0] - x 
        d_y = next_pos[1] - y
        if (d_x, d_y) == (1,0):
            return "Right"
        if (d_x, d_y) == (-1,0):
            return "Left"
        if (d_x, d_y) == (0,1):
            return "Down"
        if (d_x, d_y) == (0,-1):
            return "Up"
        
        return None

    def deduce_next_state(self, player, action):
        x,y = player["pos"]

        dx = dy = 0
        
        if action == "Right":
            dx,dy = 1,0
        if action == "Left":
            dx,dy = -1,0 
        if action == "Up":
            dx,dy = 0,-1
        if action == "Down":
            dx,dy = 0,1
        orientation = (dx,dy)

        if action in ("Interact", "Wait"):
            dx = dy = 0
            orientatin = player["orientation"]

        new_pos = (x+dx, y+dy)
        if not self.grid[new_pos]["passable"]:
            new_pos = (x,y)
        return new_pos, orientation
        

    def plan_action(self, intention, player_id, use_state=None):
        action = None

        if use_state:
            tmp_state = dict(self.grid)
            self.grid = use_state
            # TODO fix cache for distance!

        # intention = intention_state.sample()
        logger.debug("best intention to plan for: {}".format(intention))
        if intention.ID == "get_item":
            action, path = self.plan_get_item(player_id, intention.modifier)
        if intention.ID == "drop_item": 
            action, path = self.plan_drop_item(player_id)
        if intention.ID == "interact_with_pot":
            player_obj = self.get_player(player_id)
            logger.debug("interact modifier: {}".format(intention.modifier))
            pot_name, pot_pos = intention.modifier
            target_pot = intention.target
            if target_pot is None: # and use_state is not None:
                target_pot = self.grid[pot_pos]
                target_pot["pos"] = pot_pos
                # best_dist = float("inf")
                # # TODO Suboptimal, consider remembering the best pot in the intention since we already
                # # figure this out for the likelihood anyways
                # pot_objs = self.get_pots()
                # for pot in pot_objs:
                #     if not self.recipe_knowledge.pot_full(pot):
                #         dist, path = self.compute_distance(tuple(player_obj["pos"]), pot["pos"])
                #         if dist and dist < best_dist:
                #             best_dist = dist 
                #             target_pot = pot 
                #     else:
                #         if pot["occupied_by"]["ready"] and (player_obj["in_hands"] 
                #                                             and player_obj["in_hands"]["type"] == "Dish"):
                #             # TODO we kind of would need to find the closest ready pot here instead
                #             # of picking the first
                #             target_pot = pot 
                #             break
            if target_pot is None and use_state is None:
                action = None
                path = None
            else: 
                action, path = self.plan_pot(player_obj, target_pot)
        if intention.ID == "deliver_soup":
            action, path = self.plan_to_serving(player_id)
        if intention.ID == "hand_over":
            action, path = self.plan_hand_over(player_id)
        if intention.ID == "wait":
            path = []
            if random.random() > 0.8:
                action = random.choice(("Left","Right","Up","Down"))
            else:
                action = "Wait"

        if use_state:
            self.grid = tmp_state
        # action = random.choice(("Up","Down","Left","Right","Interact"))
        logger.debug("selected action: {}".format(action))
        return action, path
    
    def scale_distance(self, dist):
        """
            Convert the distance to a value between 0 and 1 which can
            be used as a probability later 
        """
        if dist is None:
            return 0
        # approx max distance of world
        positions = self.grid.keys()
        max_i = max_j = 0
        for pos in positions:
            max_i = max(max_i, pos[0])
            max_j = max(max_j, pos[1])
        # Take max_i for now, as the maximum distance is roughly max_i + max_j
        # and we would like to get values between 0.05 and 1
        return math.exp(-dist/(0.5*(max_i+max_j)))

    def shared_reachable(self, player_a, player_b):
        """
            Returns a tile object, containing the "pos" attribute of the
            closest tile that both players have access to.
        """
        a_pos = tuple(player_a["pos"])
        b_pos = tuple(player_b["pos"])

        best_dist = float("inf")
        best_el = None

        # Brute force for now
        for pos, el in self.grid.items():
            if not el["type"] == "Counter" or el["occupied_by"]:
                continue
            if not pos == a_pos and not pos == b_pos:
                d_a, path = self.compute_distance(pos, a_pos, ignore=[b_pos])
                d_b, path = self.compute_distance(pos, b_pos, ignore=[a_pos])
                if d_a is None or d_b is None:
                    continue 
                # Bad heuristic for now!
                if best_dist > d_a + d_b:
                    best_dist = d_a + d_b
                    best_el = el 
                    best_el["pos"] = pos
        return best_el

    def useable(self, pot, item):
        if self.recipe_knowledge.pot_full(pot):
            return item == "Dish"
        else:
            if item == "Dish":
                return False 
            else:
                if not pot["occupied_by"] or item in pot["occupied_by"]["flavor"]:
                    return True 
                else:
                    return False

    # @lru_cache(maxsize=100)
    def compute_distance(self, start, goal, direction=None, ignore=None):
        """
            Computes the distance between start and end using the A* algorithm.
            Has been adpated to overcooked, in that the goal tile does not need
            to be reached, but "touched" and faced in the end.
            
            Parameters
            ----------
            start: tuple
                Start position for the A*
            goal: tuple
                Goal position
                
            Returns
            -------
                int or None
                The distance from the start to the end position if a way can
                be found, otherwise None
                dict
                A dictionary containing the predecessor nodes for all visited nodes
                on the path. Can be used to build the path with this distance.
        """
        tiles = self.grid

        # We cannot use this shortcut as an occupied tile will not be passable and anything
        # on a counter is also not reachable. Instead we need to find a way to a neighbour of
        # these goal tiles.
        # if not tiles[start].get("passable", False) or not tiles[goal].get("passable", False):
        #     # Shortcut if either the start or goal tile are not passable
        #     return None, None

        allowed_tiles = set([goal])
        if ignore is None:
            ignore = []

        for tile in ignore:
            allowed_tiles.add(tile)

        if direction:
            dx, dy = direction
            if (start[0]+dx, start[1]+dy) == goal:
                return 0, [start, goal]
        came_from = {}
        cost_so_far = {}
        came_from[start] = None
        cost_so_far[start] = 0
        frontier = []
        heappush(frontier, (0, start))
        while len(frontier) != 0:
            current = heappop(frontier)[1]

            if current == goal:
                break

            # Add the neighbour if it is the goal tile
            passable_neighbours = [n_pos for n_pos in [(current[0]+x, current[1]+y) for x,y in VALID_NEIGHBOURS]
                                        if (tiles.get(n_pos, {}).get("passable",False) or n_pos in allowed_tiles)]

            for n_pos in passable_neighbours: 
                new_cost = cost_so_far[current] + 1
                if n_pos not in cost_so_far or new_cost < cost_so_far[n_pos]:
                    cost_so_far[n_pos] = new_cost
                    priority = new_cost + self._heuristic(goal, n_pos)
                    heappush(frontier, (priority, n_pos))
                    came_from[n_pos] = current

        dist = cost_so_far.get(goal, None)
        path = self.build_path(came_from, goal) if dist is not None else None

        # if dist is None:
        #     logger.debug("No path from {} to {} (costs: {})".format(start, goal, cost_so_far))

        return dist, path

    def build_path(self, came_from, goal):
        path = []
        cur = goal 
        while cur != None:
            path.append(cur)
            cur = came_from[cur]
        return path[::-1]

    def _heuristic(self, start, end):
        (x1, y1) = start
        (x2, y2) = end
        dist = abs(x1 - x2) + abs(y1 - y2)
        return dist

    def evaluate_action(self, action, intention, goal, player_id):
        """
            This function is usually called by the ToM module which would first
            set the grid to a previous state
        """

        # logger.debug("evaluate action {} for intention {}".format(action, intention))

        player_obj = self.get_player(player_id)
        other_player = self.get_other_player(player_id)
        # logger.debug("Player at {}. Other at {}".format(player_obj["pos"], other_player["pos"]))
        action_result, resulting_orientation = self.deduce_next_state(player_obj, action)

        optimal_action, optimal_path = self.plan_action(intention, player_id)

        # logger.debug("optimal action: {}".format(optimal_action))

        if action != optimal_action:
            # No need to special case "interact" since if it was not optimal to interact, 
            # interact would not have been a valid alternative. Since interact does not change position or orientation,
            # resulting distance should still be worse than doing a step
            if optimal_path:
                # wait intention will not have a path as it does not have a target
                target = optimal_path[-1]
                # recompute distance in case other player was ignored
                distance_optimal, _ = self.compute_distance(tuple(player_obj["pos"]), target, direction=tuple(player_obj["orientation"]))
                # Ignore old position
                dist, path = self.compute_distance(action_result, target, direction=resulting_orientation, ignore=[tuple(player_obj["pos"])])
                if distance_optimal is None:
                    # Optimal action ignored other agent, which blocks only path
                    distance_optimal, _ = self.compute_distance(tuple(player_obj["pos"]), target, direction=tuple(player_obj["orientation"]),
                                    ignore=[tuple(other_player["pos"])])
                    dist, path = self.compute_distance(action_result, target, direction=resulting_orientation, 
                                    ignore=[tuple(other_player["pos"]),tuple(player_obj["pos"])])
                    

                # TODO Still problematic with the potential ignoring of the other agent in case there is an alternative way.
                # The considered action may be just as good as the optimal in some cases
                # which we would miss here this way
                if distance_optimal - dist <= 0:
                    # No improvement in one step due to alternative action
                    result = -1
                else:
                    result = 1
            else:
                if optimal_action is None:
                    # No action was possible for the given intention
                    result = 0
                else:
                    result = 0.8 if action == "Wait" else 0.2 # 20% chance of perfoming a random action for intention wait

        else:
            # The same action will result in the same state, thus the same distance as the "optimal"
            result = 1


        # logger.debug("action result for {}: {}".format(action, result))

        return result


#region Likelihoods

    def classify_situation(self, player):
        reachables = list()
        types = self.get_types()
        for item_type in types:
            items = self.get_items(item_type)
            for item in items:
                if self.reachable(tuple(player["pos"]), tuple(item["pos"])):
                    reachables.append(item)
        
        return reachables

        
    def in_reachables(self, reachables, _type):
        for el in reachables:
            if _type in el["type"]:
                return True 
        return False


    def get_item_likelihood(self, item_type):
        if item_type != "Dish" and not self.recipe_knowledge.requires_ingredient(self.orders, item_type):
            return 0
        pots = self.get_pots()
        for pot in pots:
            if self.recipe_knowledge.pot_full(pot):
                if item_type == "Dish":
                    return 1
            else:
                if not pot["occupied_by"] or (pot["occupied_by"] and pot["occupied_by"]["flavor"] == item_type):
                    return 1

        return 0

    def get_pot_likelihood(self, carried_obj, reachables):
        if "Soup" in carried_obj["type"]:
            return 0


        for obj in reachables:
            if obj["type"] == "Pot":
                pot = obj
                if not self.recipe_knowledge.pot_full(pot):
                    if not pot["occupied_by"] or (pot["occupied_by"] and pot["occupied_by"]["flavor"] == carried_obj["type"]): 
                        return 1
                else:
                    if pot["occupied_by"] and pot["occupied_by"]["ready"] and carried_obj["type"] == "Dish": 
                        # TODO Should this really only trigger when soup ready?
                        return 1
        return 0


    def likelihood(self, intention, player_id):
        player = self.get_player(player_id)
        reachables = self.classify_situation(player)

        if intention.ID == "get_item":
            if player["in_hands"] != None or not self.in_reachables(reachables, intention.modifier):
                return 0

            return self.get_item_likelihood(intention.modifier)
        if intention.ID == "interact_with_pot":
            if player["in_hands"] is None or not self.in_reachables(reachables, "Pot"):
                logger.debug("interact short circut. in hands: {}".format(player["in_hands"]))
                return 0
            if player["in_hands"] and "Soup" in player["in_hands"]["type"]:
                return 0

            return self.get_pot_likelihood(player["in_hands"], reachables)
        if intention.ID == "deliver_soup":
            if not (player["in_hands"] and "Soup" in player["in_hands"]["type"]):
                return 0

            return 1

        #Consider if we need the modifiers for these likelihood?
        if intention.ID == "hand_over":
            if player["in_hands"] is None:
                return 0 

            return 0.005 #self.hand_over_likelihood(intention.modifier, self.env_id)
        if intention.ID == "drop_item":
            if player["in_hands"] is None:
                return 0
            return 0.01 #self.drop_item_likelihood(intention.modifier,)
        if intention.ID == "wait":
            return 0.01


#endregion
