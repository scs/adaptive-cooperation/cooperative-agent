from numpy.linalg import norm as np_norm
from numpy import dot as np_dot, sqrt as np_sqrt, inf as np_inf
from numpy import log as np_log, sum as np_sum, e as np_e, pi as np_pi, exp as np_exp
from numpy import abs as np_abs, mean as np_mean, var as np_var, max as np_max, clip as np_clip, argmax as np_argmax
from numpy import argmin as np_argmin, cos as np_cos
from scipy.stats import entropy as np_entropy

import numpy as np
from copy import copy
import sys

# from numba import jit, vectorize, void, int8, float64, typeof, types


def free_energy(P, Q):
    """ see Friston (2012) 
    My interpretation in differences between perception and active inference:
    - In perception, the posterior is the new posterior after perception.
    - In active inference, the posterior is the expected/intended distribution, with the
        new posterior after perception as the prior.
    """
    with np.errstate(all='raise'):
        try:
            # PE = Q - P  # prediciton-error
            surprise = np_entropy(P) #  if np_dot(PE, PE) > 0 else 0.
            surprise = surprise if abs(surprise) != np_inf else 0.
            cross_entropy = np_entropy(P, Q)
            cross_entropy = cross_entropy if abs(cross_entropy) != np_inf else 0.
            c_e = 1/(1+np_exp(-4*(cross_entropy - 0.5)))  # should be maxing out at 1
            F = surprise + c_e

            return F, surprise, c_e, surprise+cross_entropy
        except Exception as e:
            raise Exception("RuntimeWarning in free_energy(P,Q):", str(e), "#P:", len(P), "#Q:", len(Q))


def precision(PE):
    """ Calculate precision as the inverse variance of the updated prediction error.

    return updated precision and updated average_free_energy
    """
    with np.errstate(all='raise'):
        try:
            variance = np_var(PE)  # np_var(PE, ddof=1)  # mad(PE)
            variance = variance if variance > 0.00001 else 0.00001 # so log(var) should max at -5
            pi = np_log(1. / variance)  # should max at 5
            new_precision = 1/(1+np_exp(-(pi - 2.5))) # should be about max. 1

            return new_precision  # , variance
        except Exception as e:
            raise Exception("RuntimeWarning in precision(PE):", str(e), "PE:", PE) from e


def norm_dist(distribution, smooth=True):
    """ Normalize distribution, and apply add-one smoothing to leave
    unused probability space.
    """
    smoothing_parameter = 0.0001

    # if smooth:
    #     add_one_smoothing = smoothing_parameter
    #     norming_factor = np_sum(distribution[:, 0] + add_one_smoothing) 

    #     distribution[:, 0] = (distribution[:, 0] + add_one_smoothing) / norming_factor
    # else:
    #     distribution[:, 0] = distribution[:, 0] / np_sum(distribution[:, 0])
    if smooth:
        add_one_smoothing = smoothing_parameter
        norming_factor = np_sum(distribution + add_one_smoothing) 

        distribution = (distribution + add_one_smoothing) / norming_factor
    else:
        distribution = distribution / np_sum(distribution)
    return distribution


def kalman_gain(F, pi, oldK=None, gain_gain=0.5):
    """ Calculate the Kalman gain from free-energy and precision of the layer.

    Examples:
    low pi => steep response in K to small increases in F, max K = 1.0 (strong prediction-error influence)
    high pi => slow response in K to strong increases in F, max K = 0.5 (mostly preserved prior)
    """
    # pi = pi if pi < 5. else 5.  # limit precision variance
    K = F / (F + pi)  # gain factor from free-energy and precision

    if oldK is not None:
        # filter the Kalman Gain over time using a "gain gain" ;)
        # high gain_gain means stronger fluctuations from K
        K, _ = kalman_filter(oldK, K, gain_gain)

    return K


def kalman_filter(prior, observation, K):
    """ Calculate two iteration kalman filter with given measurement variance.
    The higher the variance the more likely will the prior be preserved.

    Examples:
    low pi => steep response in K to small increases in F, max K = 1.0 (strong prediction-error influence)
    high pi => slow response in K to strong increases in F, max K = 0.5 (mostly preserved prior)

    Returns the posterior estimate.
    """

    # calculate precision from ascending prediction-error
    prediction_error = observation - prior
    xhat = prior + K * (prediction_error)  # posterior estimate

    return xhat, prediction_error


def multisignal_kalman_filter(prior, observation_gain_tuples):
    posterior = copy(prior)
    for gain, obs in observation_gain_tuples:
        xhat = posterior + gain * (obs - posterior)
        posterior = xhat

    return posterior


def inhibition_belief_update(P, Q, K, tom_Q=None, tom_K=0.5):
    """ Calculate kalman filter with given free_energy and precision for gain factor.
    The higher the precision the more likely will the prior be preserved.

    P = Prior, Q = Posterior, K = Kalman gain

    Returns the update belief estimate.
    """
    H = copy(P)

    if tom_Q is not None:
        observation_gain_tuples = []

        # add observation gain tuple of current posterior to observation_gain_tuples
        # observation_gain_tuples.append((K, Q))
        # add an observation gain tuple for the inferred belief distribution
        observation_gain_tuples.append((tom_K, tom_Q))

        observation_gain_tuples.append((K, Q))

        # multisignal kalman update
        H = multisignal_kalman_filter(P, observation_gain_tuples)
        H = norm_dist(H, smooth=True)
    else:
        # simple kalman update
        H, _ = kalman_filter(P, Q, K)
        H = norm_dist(H, smooth=True)

    return H
