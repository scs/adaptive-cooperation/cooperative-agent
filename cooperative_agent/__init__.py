
__title__ = 'Cooperative Agent'
__version__ = '0.01'

from .hierarchy import *


"""
    Define an agent class that holds
    - State: Hierarchical representation of knowledge and desire/intention
        For each hierarchy level (recipe desire and subtask intention) a probability distribution
    - Action planner that takes the current subtask intention and determines the next action for the agent (including no-ops)
    - reference to "other agent" which is an agent model containing deltas and specific update procedures
    - A reference to the hierarchy where the actual update steps are defined
    - The relevant update functions defining how top-down and buttom-up information should be merged to generate the new states
    for each hierarchy layer

    General update loop:
    - Agent receives new information from environment
    - Agent updates its model about the other by running the hierarchy with the other's update functions and state
    - Agent updates its own state by running the hierarchy with its own state (and the predictions regarding the other)
    - Agent uses the action planner to decide on the next action to take
    - Agent sends the action to the environment

    ToM Inference:
    - Start: Cooperativeness/dominance of the other agent
        - 
    - Later: Learning of subtasks (and recipes) for oneself (and thus also the other)
    - Unlikely: Consider different state perception of the other agent
        - E.g. does not know where a certain ingrediant is
        -> No reason to believe that this missconception remains true
            - Unless it is never shown (potentially consider this case after all)

"""