import logging, time, threading, json
logger = logging.getLogger(__name__)
import zmq

from connectionManager import ConnectionManager, get_params

class IOControl(object):

    def __init__(self, agent, setup_player_connection, config=None):
        if not config:
            config = {}
        self.setup_player_connection = setup_player_connection
        self.state_observers = []
        self.agent_callback = None
        self.agent = agent
        self.con_man = ConnectionManager()
        self.connection_identifier = config.get("identifier", "agent_receiver")
        self.own_port = config.get("ctrl_port", "6664")
        self.callback = None
        server_params = get_params("zmq", "incoming")
        server_params.update({
            "connection_identifier": self.connection_identifier,
            "port": self.own_port,
        })
        self.con_man.open_connection("zmq", server_params, callback=self.handle_messages)
        # Notify model to setup the connection
        receiver_params = {
            "servername": "localhost",
            "port": "6666", #Environment server port
            "protocol": "tcp"
        }
        connection_message = {
                            "type": "setup_connection",
                            "connection_type": "acting_observer",
                            "identifier": self.connection_identifier,
                            "desired_agent_id": config.get("desired_agent_id", 0),
                            "connection_params": {"protocol": "tcp", "port": self.own_port}
                        }
        self.con_man.send_message("zmq", receiver_params, 
                         connection_message)

        # self.register_state_observer("stateClient_webviz")
        # client_params = get_params("zmq", "outgoing")
        # client_params.update({
        #     "connection_identifier": "stateClient_webviz",
        #     "port": "6667",
        #     "client_ident": "stateClient_webviz{}".format(config["desired_agent_id"])
        # })

        # self.con_man.open_connection("zmq", client_params, self.handle_con_messages)

    def clean_up(self, env_id):
        self.con_man.notify(self.connection_identifier, {"type": "agentDisconnect", "identifier": self.connection_identifier, "id": env_id})
        time.sleep(0.3)
        self.con_man.disconnect()

    def register_state_observer(self, remote_ident):
        logger.info("registering state observer {}".format(remote_ident))
        if not remote_ident in self.state_observers:
            self.state_observers.append(remote_ident)
        # self.con_man.add_connection()

    def notify_state_observers(self, state_msg):
        for observer in self.state_observers:
            # logging.debug("sending state to {}: {}".format(observer, state_msg))
            self.con_man.notify(observer, state_msg)

    def handle_con_messages(self, msg):
        logger.info("received con message {}".format(msg))

    def handle_messages(self, msg):
        # logger.debug("Agent received message: {}".format(msg))
        action = None

        if msg["type"] == "agentAccepted":
            self.setup_player_connection(msg["agent_id"])
            return

        if msg["type"] == "agentDenied":
            logger.error("Agent could not register at environment")
            import sys 
            sys.exit(1)

        if msg["type"] == "agentStateRequest":
            logger.debug("State request received")
            con_name = "stateClient_" + msg["identifier"]
            self.register_state_observer(con_name)
            client_params = get_params("zmq", "outgoing")
            client_params.update({
                "connection_identifier": con_name,
                "port": msg["connection_params"]["port"],
                "client_ident": "{}_{}".format(con_name, self.agent.env_id)
            })
            self.con_man.open_connection("zmq", client_params, self.handle_con_messages)

            # self.con_man.notify("webviz", {"type": "agentState", "state": "dummy"})

            # We will most likely need to create a ZMQConnection (similar to what I do in the environment)
            # here in order to make this work, as the webviz also binds a ROUTER and I do not believe two bound
            # Routers can send messages to each other -.- 


        if msg["type"] == "overcookedState":
            # if "agent_id" in msg["info"]:
            #     self.setup_player_connection(msg["info"]["agent_id"])
            if self.callback:
                player_id, action = self.callback(msg)

        if action:
            self.con_man.notify(self.connection_identifier, {"type": "agentAction", "id": player_id, "action": action})